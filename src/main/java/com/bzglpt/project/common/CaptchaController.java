package com.bzglpt.project.common;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.security.Key;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.annotation.Resource;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import com.bzglpt.common.utils.StringUtils;
import com.bzglpt.common.utils.sign.RSACode;
import com.bzglpt.project.system.service.ISysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.google.code.kaptcha.Producer;
import com.bzglpt.common.constant.Constants;
import com.bzglpt.common.utils.IdUtils;
import com.bzglpt.common.utils.sign.Base64;
import com.bzglpt.framework.redis.RedisCache;
import com.bzglpt.framework.web.domain.AjaxResult;

import static com.bzglpt.common.utils.ZhglptUtils.getConfigCacheKey;

/**
 * 验证码操作处理
 *
 * @author bzglpt
 */
@RestController
public class CaptchaController
{
    @Resource(name = "captchaProducer")
    private Producer captchaProducer;

    @Resource(name = "captchaProducerMath")
    private Producer captchaProducerMath;

    @Autowired
    private RedisCache redisCache;

    // 验证码类型
    @Value("${bzglpt.captchaType}")
    private String captchaType;

    @Autowired
    private ISysConfigService configService;

    /**
     * 生成验证码
     */
    @GetMapping("/captchaImage")
    public AjaxResult getCode(HttpServletResponse response) throws IOException
    {
        // 保存验证码信息
        String uuid = IdUtils.simpleUUID();
        String verifyKey = Constants.CAPTCHA_CODE_KEY + uuid;

        String capStr = null, code = null;
        BufferedImage image = null;

        //从缓存服务器获取验证码类型
        String captchaType = redisCache.getCacheObject(getConfigCacheKey(Constants.SYS_CONFIG_CAPTCHATYPE));
        if(StringUtils.isEmpty(captchaType)){
            captchaType = configService.selectConfigByKey(Constants.SYS_CONFIG_CAPTCHATYPE);
        }
        // 生成验证码
        if ("math".equals(captchaType))
        {
            String capText = captchaProducerMath.createText();
            capStr = capText.substring(0, capText.lastIndexOf("@"));
            code = capText.substring(capText.lastIndexOf("@") + 1);
            image = captchaProducerMath.createImage(capStr);
        }
        else if ("char".equals(captchaType))
        {
            capStr = code = captchaProducer.createText();
            image = captchaProducer.createImage(capStr);
        }

        redisCache.setCacheObject(verifyKey, code, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        // 转换流信息写出
        FastByteArrayOutputStream os = new FastByteArrayOutputStream();
        //生成公钥和私钥存储
        Map<String, Key> keyMap;
        //公钥发给前端
//        String publicKey;
        try
        {
            ImageIO.write(image, "jpg", os);
            //生成公钥和私钥
//            keyMap = RSACode.initKey();
//            publicKey = RSACode.getPublicKey(keyMap);
////            byte[] bytes = RSACode.encryptByPublicKey("1234", publicKey);
//            String privateKey = RSACode.getPrivateKey(keyMap);
////            System.out.println(new String(RSACode.decryptByPrivateKey(bytes,privateKey)));
//            //存储私钥
//            redisCache.setCacheObject("privateKey-"+uuid, privateKey, Constants.CAPTCHA_EXPIRATION, TimeUnit.MINUTES);
        }
        catch (Exception e)
        {
            return AjaxResult.error(e.getMessage());
        }

        AjaxResult ajax = AjaxResult.success();
        ajax.put("uuid", uuid);
        ajax.put("img", Base64.encode(os.toByteArray()));
//        ajax.put("publicKey", publicKey);
        return ajax;
    }
}
