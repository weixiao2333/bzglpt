package com.bzglpt.project.zhgl.controller;

import com.bzglpt.common.response.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.Duration;
import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/test")
public class TestDemoController {

    //测试接口
    @GetMapping("/demo")
    public R test(){
        //五分钟之前（和北京时间差8小时）
        Instant now = Instant.now().minus(Duration.ofMinutes(5));

        Map<String, Object> map = new HashMap<>();
        map.put("aa",11);
        map.put("aa1",112);
        return R.ok();
    }
}
