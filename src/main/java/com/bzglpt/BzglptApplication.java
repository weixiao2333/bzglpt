package com.bzglpt;

import com.bzglpt.framework.config.ServerConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * 启动程序
 * @author bzglpt
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class BzglptApplication {
    public static void main(String[] args){
        // System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(BzglptApplication.class, args);
        System.out.println("(♥◠‿◠)ﾉﾞ  项目启动成功   ლ(´ڡ`ლ)ﾞ  \n" +
                " __         __  __     ______  \n" +
                "/\\ \\       /\\ \\_\\ \\   /\\__  _\\ \n" +
                "\\ \\ \\____  \\ \\____ \\  \\/_/\\ \\/ \n" +
                " \\ \\_____\\  \\/\\_____\\    \\ \\_\\ \n" +
                "  \\/_____/   \\/_____/     \\/_/ \n" +
                "                               ");
        System.out.println("项目访问链接：http://localhost:" + ServerConfig.getPort() + "/");
    }
}
