package com.bzglpt.common.response;

import lombok.Data;
import java.util.HashMap;
import java.util.Map;

/**
 * 程序接口统一返回信息封装
 */
@Data
//@Accessors(chain = true)//允许链式操作
public class R {

    private Integer code;//响应码
    private String msg;//响应消息
    private Map<String, Object> data =  new HashMap<>();

    public static R ok(){
        R r = new R();
        r.setCode(0);
        r.setMsg("成功");
        return r;
    }

    public static R error(){
        R r = new R();
        r.setCode(-1);
        r.setMsg("失败");
        return r;
    }

    public R data(String key, Object value){
        this.data.put(key, value);
        return this;
    }
}
