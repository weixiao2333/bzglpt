package com.bzglpt.common.utils;


import com.bzglpt.common.constant.Constants;

/**
 * 综合管理平台工具类
 */
public class ZhglptUtils {

    /**
     * 设置sys_config cache key
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getConfigCacheKey(String configKey)
    {
        return Constants.SYS_CONFIG_KEY + configKey;
    }

    /**
     * 设置sys_dict cache key
     * @param configKey 参数键
     * @return 缓存键key
     */
    public static String getCacheKey(String configKey)
    {
        return Constants.SYS_DICT_KEY + configKey;
    }
}
