package com.bzglpt.framework.config;

import javax.servlet.http.HttpServletRequest;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import com.bzglpt.common.utils.ServletUtils;

/**
 * 服务相关配置
 *
 * @author bzglpt
 */
@Component
@ConfigurationProperties(prefix = "server")
public class ServerConfig
{
    /** 启动端口 */
    private static String port;
    /**
     * 获取完整的请求路径，包括：域名，端口，上下文访问路径
     *
     * @return 服务地址
     */
    public String getUrl()
    {
        HttpServletRequest request = ServletUtils.getRequest();
        return getDomain(request);
    }

    public static String getDomain(HttpServletRequest request)
    {
        StringBuffer url = request.getRequestURL();
        String contextPath = request.getServletContext().getContextPath();
        return url.delete(url.length() - request.getRequestURI().length(), url.length()).append(contextPath).toString();
    }

    public static String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }
}
