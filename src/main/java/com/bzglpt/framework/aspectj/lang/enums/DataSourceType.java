package com.bzglpt.framework.aspectj.lang.enums;

/**
 * 数据源
 *
 * @author bzglpt
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
