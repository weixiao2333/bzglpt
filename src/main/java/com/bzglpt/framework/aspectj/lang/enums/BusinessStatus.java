package com.bzglpt.framework.aspectj.lang.enums;

/**
 * 操作状态
 *
 * @author bzglpt
 *
 */
public enum BusinessStatus
{
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
